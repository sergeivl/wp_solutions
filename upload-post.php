<?php

/*
* @author SergeiVL
*/

require_once( dirname(__FILE__) . '/wp-load.php' );

$post_data = array(
	'post_title'    => 'Новый тестовый пост с дополнительным полем',
	'post_content'  => 'Контент тестового поста с дополнительным полем',
	'post_status'   => 'publish',
	'post_author'   => 1,
	'post_category' => array(1)
);

// Вставляем запись в базу данных
$post_id = wp_insert_post($post_data, true);
print_r($post_id);

// Задаём значение для дополнительного поля:
// В одном из моих блогов есть дополнительное поле rating (числовое).
// Его мы и зададим. Для примера, выставим значение 80
update_post_meta($post_id , 'rating', 80);

// Второе поле - строковое - postscriptum
update_post_meta($post_id , 'postscriptum', 'Спасибо за внимание. Подписывайтесь на мой блог');

// Для примера возьмём картинку с моего же блога, которая была залита вне структуры wordpress
$url = 'http://sergeivl.ru/public/img/svlJForm.png';

// Прикрепим к ранее сохранённому посту
//$post_id = 3061;
$description = "Картинка для обложки";

// Установим данные файла
$file_array = array();
$tmp = download_url($url);

// Получаем имя файла
preg_match('/[^\?]+\.(jpg|jpe|jpeg|gif|png)/i', $url, $matches );
$file_array['name'] = basename($matches[0]);
$file_array['tmp_name'] = $tmp;

// загружаем файл
$media_id = media_handle_sideload( $file_array, $post_id, $description);

// Проверяем на наличие ошибок
if( is_wp_error($media_id) ) {
	@unlink($file_array['tmp_name']);
	echo $media_id->get_error_messages();
}

// Удаляем временный файл
@unlink( $file_array['tmp_name'] );

// Файл сохранён и добавлен в медиатеку WP. Теперь назначаем его в качестве облож
set_post_thumbnail($post_id, $media_id);
